const { uuid } = require('uuidv4');
const uuidv4ID = uuid();
const cron = require('node-cron');
const moment = require('moment');
const fs = require('fs-extra');

const dir = './offlineStorage';

const deviceIP = 'http://192.168.50.119';
const deviceID = 'b176cf1e-a537-46c9-8ae7-e7b27a3de5c7';
const customerName = '';

let pushDataMoisture = [];
let pushDataQuotient = [];
let pushDataTemperature = [];
let pushDataCombined = [];
let internetPushSuccess = false;
const pushDataMoistureQuotientTemperature = [];

const responseStatus200 = '200';
const responseStatus201 = '201';

// Gan's Mock API Gateway

const axios = require('axios');
const { aws4Interceptor } = require('aws4-axios');
const axiosSolar = {
	baseURL: 'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
	apiKey: 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
	region: 'ap-southeast-1',
	service: 'execute-api',
	accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
	secretAccessKey: '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',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { 'x-api-key': axiosSolar.apiKey },
});
const interceptor = aws4Interceptor(
	{
		region: axiosSolar.region,
		service: axiosSolar.service,
	},
	{
		accessKeyId: axiosSolar.accessKeyId,
		secretAccessKey: axiosSolar.secretAccessKey,
	}
);
axiosMainHelper.interceptors.request.use(interceptor);

const axiosLocal = axios.create({
	baseURL: deviceIP,
	headers: {
		'Content-Length': 0,
		'Content-Type': 'text/plain',
	},
	responseType: 'text',
});

// error checking
const returnError = (err) => {
	return !!err.body ? JSON.stringify(err.body) : JSON.stringify(err.message);
};

// calculation formula, median
function medianOfArray(arr) {
	console.log('array value: ', arr);
	const middle = (arr.length + 1) / 2;
	console.log();

	// Avoid mutating when sorting
	const sorted = [...arr].sort((a, b) => {
		return a - b;
	});
	console.log('sorted', sorted);
	const isEven = sorted.length % 2 === 0;

	return isEven
		? (sorted[middle - 1.5] + sorted[middle - 0.5]) / 2
		: sorted[middle - 1];
}

// random number generator
function randomIntFromInterval(min, max) {
	// min and max included
	return Math.floor(Math.random() * (max - min + 1) + min);
}

// const rndInt = randomIntFromInterval(1, 20);
// console.log(rndInt);

// read data from Ludwig moisture sensor
const readData = async (time) => {
	console.log('start');
	readTask = cron.schedule(`*/${time} * * * * *`, async () => {
		if (pushDataMoistureQuotientTemperature.length <= 11) {
			// let moisture = { data: 0 };
			// let quotient = { data: 0 };
			// let temperature = { data: 0 };

			try {
				const moisture =
					parseFloat(
						await axiosLocal.post('/param01.html', '__SL_P_UL1=FMG')
					) || 0;
				const quotient =
					parseFloat(
						await axiosLocal.post('/param01.html', '__SL_P_UL1=QUO2')
					) || 0;
				const temperature =
					parseFloat(
						await axiosLocal.post('/param01.html', '__SL_P_UL1=TEMP')
					) || 0;

				// let reading1 = parseFloat(moisture.data) || 0;
				// let reading2 = parseFloat(quotient.data) || 0;
				// let reading3 = parseFloat(temperature.data) || 0;
				pushDataMoisture.push(moisture);
				pushDataQuotient.push(quotient);
				pushDataTemperature.push(temperature);
				// pushDataMoistureQuotientTemperature.push({
				// 	moisture,
				// 	quotient,
				// 	temperature,
				// });

				console.log(
					'pushDataMoisture',
					pushDataMoisture,
					pushDataQuotient,
					pushDataTemperature,
					pushDataMoistureQuotientTemperature
				);
				// console.log('pushDataMoisture.length', pushDataMoisture.length);
				// console.log('pushDataQuotient.length', pushDataQuotient.length);
				// console.log('pushDataTemperature.length', pushDataTemperature.length);
				// console.log(pushDataQuotient);
				// console.log(pushDataTemperature);
			} catch (err) {
				console.log(returnError(err));
			}
			if (
				pushDataMoisture.length == 11 &&
				pushDataQuotient.length == 11 &&
				pushDataTemperature.length == 11
			) {
				pushDataCombined.push({
					moisture: await medianOfArray(pushDataMoisture),
					quotient: await medianOfArray(pushDataQuotient),
					temperature: await medianOfArray(pushDataTemperature),
				});
			}
			// 	if (
			// 		pushDataMoisture.length == 11 &&
			// 		pushDataQuotient.length == 11 &&
			// 		pushDataTemperature.length == 11
			// 	) {
			// 		const outputPushMoistureData = await medianOfArray(pushDataMoisture);
			// 		const outputPushQuotientData = await medianOfArray(pushDataQuotient);
			// 		const outputPushTemperatureData = await medianOfArray(
			// 			pushDataTemperature
			// 		);
			// 		let outputData = {
			// 			moisture: outputPushMoistureData,
			// 			quotient: outputPushQuotientData,
			// 			temperature: outputPushTemperatureData,
			// 		};
			// 		// console.log('outputData', outputData);
			// 		pushDataCombined.push(outputData);
			// 		console.log(pushDataCombined);
			// 	}
		} else {
			pushDataCombined.length = 0;
			pushDataMoisture.length = 0;
			pushDataQuotient.length = 0;
			pushDataTemperature.length = 0;
			pushDataMoistureQuotientTemperature.length = 0;
		}
	});
};

// push to cloud function
const pushToCloud = async () => {
	pushTask = cron.schedule(`0 * * * * *`, async () => {
		checkLocalFile();
		// console.log('info', info);
		// if (pushDataCombined.length >= 0) {
		//   const info = pushDataCombined;
		if (pushDataCombined.length == 1) {
			// push at the 0 sec of every minute

			const output = {
				deviceID: deviceID,
				timestamp: moment().unix(),
				sensor: {},
			};
			output.sensor = pushDataCombined[0];

			const data = { id: uuidv4ID, data: JSON.stringify(output) };
			console.log('data', data);
			try {
				const response = await axiosMainHelper.post('/rawtmpdata', data);

				if (
					response.status == responseStatus200 ||
					response.status == responseStatus201
				) {
					console.log('push success');
					internetPushSuccess = true;
				} else {
					console.log('push not success');
					writeToLocal(data);
				}
				console.log(response.status);
				console.log(data);
			} catch (err) {
				writeToLocal(data);
				console.log('push not success');
				console.log(returnError(err));
			}
			// }
			pushDataCombined.length = 0;
		}
	});
};

const writeToLocal = async (data) => {
	if (!(await fs.existsSync(dir))) {
		await fs.mkdirSync(dir);
	}
	// internetPushSuccess  = true , if can push , response.status == 201. internetPushSuccess  = false, response.status != 201.
	console.log('Checking if need write to local: ', !internetPushSuccess);
	if (!internetPushSuccess) {
		try {
			await fs.writeJSONSync(`${dir}/${moment().unix()}.json`, data);
		} catch (e) {
			console.log(returnError(err));
		}
	}
};

const checkLocalFile = async () => {
	if (await fs.existsSync(`${dir}`)) {
		await fs.readJSONSync(`${dir}`, (err, content) => {
			if (err) {
				console.log(returnError(err));
			}
			if (content) {
				try {
					const data = JSON.parse(content);
					const response = await axiosMainHelper.post('/rawtmpdata', data);
					if (
						response.status == responseStatus200 ||
						response.status == responseStatus201
					) {
						internetPushSuccess = true;
						console.log('push success');
					} else {
						writeToLocal(data);
						console.log('push not success');
					}
					console.log(response.status);
					console.log(data);
				} catch (err) {
					writeToLocal(data);
					console.log(returnError(err));
					console.log('push not success');
				}
			}
		});
	}
};

readData(5); // read data every 5 seconds

pushToCloud();
